import React from 'react';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import styled from 'styled-components';

const AccountDropDown = styled(Popover)`
    top: 60px !important;
`;

const Arrow = styled.div `
    width: 0;
    height: 0;
    top: 3px;
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
    border-bottom: 8px solid rgb(255, 255, 255);
    position: relative;
    left: 82%;
    opacity: ${props=>props.open ? 1 : 0};
    transition: transform 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, opacity 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    z-index: 5000;
`;

const propTypes = {
    element: React.PropTypes.object,
    menuItems: React.PropTypes.array,
    onClose: React.PropTypes.func,
    open: React.PropTypes.func,
    userEmail: React.PropTypes.string
};


const AccountMenu = ({
    onClose,
    open,
    element,
    userEmail,
    menuItems
}) => (
    <Arrow open={open}>
        <AccountDropDown open={open}
                         anchorEl={element}
                         anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                         targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                         onRequestClose={onClose}>
            {menuItems?
                <Menu>
                    {menuItems.map( item => {return item; })}
                </Menu>
                :null
            }
        </AccountDropDown>
    </Arrow>
);

AccountMenu.propTypes = propTypes;

export default AccountMenu;