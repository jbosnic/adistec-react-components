import React from 'react';
import styled from 'styled-components';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'react-select-adistec';
import { connect } from 'react-redux';
import { Field,change,untouch } from 'redux-form';
import 'react-select-adistec/dist/react-select';
import { FormattedMessage } from 'react-intl';
import ClearButtonSelectField from './ClearButtonSelectField.jsx';
import './SelectField.scss';
import isEmpty from 'lodash/isEmpty';

const StyledSelect = styled(SelectField)`
    width: 100% !important;
    box-sizing: border-box !important;
    height: 61px !important;

    /* Select floating label */
    label {
        top: 45% !important;
        font-size:12px;
    }
    
    /* Select content */
    div > div {
        bottom: 10px !important;
        margin-top: 0 !important;
        padding-right: 36px;
        font-size: 14px !important;
    }

    /* Select arrow */
    button {
        bottom: 10px !important;
    }

    /* Select error field */
    div:nth-child(4) {
        bottom: 18px !important;
    }
`;

const StyledInput = styled.span`

`;

const StyledItem = styled.span`
    white-space:nowrap;
    position:relative;
`;

const StyledSelectedItem = styled(StyledItem)`
    padding-left:5px;

    i{
        display:block;
        position:absolute;
        top:-8px;
        bottom:-8px;
        left:-10px;
        border-left:solid 3px #008fcb !important;
    }

`;

const renderMultipleOption = (option,i,inputValue,valueArray) => {
    if(valueArray.filter(valueElement => valueElement.value == option.value).length > 0){
        return <StyledSelectedItem><i />{option.label}</StyledSelectedItem>;
    }
    else {
        return <StyledItem>{option.label}</StyledItem>;
    }
};

const RenderSelectInput = (props) => (
    <div className={!isEmpty(props.input.value) ? 'item-selected' : ''}>
        <label className="select--multi-floatinglabel">{props.label}</label>
        <SelectField
            {...props}
            className={props.meta.touched && props.meta.error ? 'error' : null}
            customSelectedItemTemplate={props.multi ? 
                (values) => {
                    if(values.length > 1)
                       {return <StyledInput>{values.length + ' '}{<FormattedMessage id="selectField.multiSelectInputMessage"/>}</StyledInput>;}
                    else
                        {return <StyledInput>{values[0].label}</StyledInput>;}
                } : null }
            renderOption={props.multi && !props.renderOption ? props.renderMultipleOption : props.renderOption}
            value={props.input.value}
            onChange={(value) => {
                return props.multi ? props.input.onChange(value.map(p => p.value)) :
                    props.input.onChange(value ? value.value : '');
            }}
            onBlur={() => {
                if(props.multi) return props.input.onBlur([...props.input.value]);}
            }
            onValueClick={() => null}
            closeOnSelect={props.multi ? false : true}
            clearable={false}
            //clearValueText=""
        />
        {props.meta.touched && props.meta.error ? <span className="error">{props.meta.error}</span> : null}
    </div>

);

class AdistecSelect extends React.Component {
    constructor(props) {
        super(props);
    }

    reset = () => {
        this.props.resetSelectField(this.refs[this.props.name].context._reduxForm.form,this.props.name,null);
        this.props.untouchSelectField(this.refs[this.props.name].context._reduxForm.form,this.props.name);
    }

    render(){
        return(
            <div className={`${this.props.size ? 'adistec-select-field col s' + this.props.size : ''} ${this.props.multiple ? 'multiple' : ''}`} style={{ position: 'relative' }}>
                <Field floatingLabelText={this.props.label}
                        onBlurResetsInput={this.props.onBlurResetsInput}
                        onSelectResetsInput={this.props.onSelectResetsInput}
                        component={RenderSelectInput}
                        className={this.props.className}
                        onChange={this.handleChange}
                        placeholder={null}
                        closeOnSelect={this.props.closeOnSelect}
                        multi={this.props.multi}
                        searchable={false}
                        backspaceRemoves={false}
                        removeSelected={false}
                        maxSearchResults = {this.props.maxSearchResults}
                        noResultsText={this.props.noResultsText}
                        customSelectedItemTemplate={this.props.customSelectedItemTemplate}
                        optionRenderer={this.props.renderOption}
                        validate={this.props.validate}
                        withRef={this.props.clearButton ? true : false}
                        ref={this.props.clearButton ? this.props.name : null}
                        name={this.props.name}
                        options={this.props.items ? this.props.items : null}
                        pageSize={this.props.maxSearchResults? this.props.maxSearchResults : 5 }
                        noResultsText={this.props.noResultsText}
                        searchable={this.props.autocomplete}
                       label={this.props.label}
                />
                 {this.props.clearButton ? <ClearButtonSelectField onClick={this.reset}/> : null}
            </div>
        );
    }
}

AdistecSelect.defaultProps = {
    renderOption:renderMultipleOption,
    onBlurResetsInput:true,
    onSelectResetsInput:true,
    noResultsText: <FormattedMessage id="autocomplete.noResultsFound"/>,
    maxSearchResults: 5,
    autocomplete:false
};

export default connect(null, {
  resetSelectField: (formName,fieldName,value) => change( formName, fieldName, value ),
  untouchSelectField: (formName,fieldName) => untouch(formName,fieldName)
})(AdistecSelect);
