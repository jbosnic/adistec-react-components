import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover/Popover';
import { Menu, MenuItem } from 'material-ui/Menu';
import './FeedbackButton.scss';
import { injectIntl } from 'react-intl';

const propTypes = {
    handleSubmit: React.PropTypes.promise
};


class AdistecFeedbackButton extends React.Component {
    
    static contextTypes = {
      intl: React.PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            showMessage: false,
            open: false,
            anchorOrigin: {
                horizontal: 'right',
                vertical: 'top',
            },
            targetOrigin: {
                horizontal: 'right',
                vertical: 'bottom',
            },
            feedbackComments:''
        };
    }

    handleClick = (event) => {
        event.preventDefault();
        this.setState({ open: true,anchorEl: event.currentTarget, });
    };

    handleRequestClose = () => {
        this.setState({
            open: false,showMessage: false
        });

    };

    handleSubmit = () => {
        if(this.state.feedbackComments){
            this.props.handleSubmit(this.state.feedbackComments)
            .then(response => {
                this.setState({ error:false });
            })
            .catch(error => {
                this.setState({ error:true });
            }).finally(() => {
                this.setState({ showMessage:true });
            });
        }                
    }

    handleTextAreaChange = (event) => {
        this.setState({ feedbackComments:event.target.value });
    }

    render() {
        return (
            <div className="adistec-feedback-button">
                <RaisedButton onClick={this.handleClick} label={this.context.intl.formatMessage({ id:'feedbackButton.title' })}/>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={this.state.anchorOrigin}
                    targetOrigin={this.state.targetOrigin}
                    onRequestClose={this.handleRequestClose}
                    className="feedback-dialog"
                >
                    <Menu>
                      {
                        this.state.showMessage ?
                          <div style={{ width: '250px',height: '150px',fontSize: '13px',padding: '10px 13px',textAlign: 'center', margin: '0 auto', display: 'table' }}>
                           <p style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                           <strong style={{ display: 'block',fontSize: '16px', color: this.state.showMessage && !this.state.error ? '#73ae77' : '#e74c3c' }}>
                           {this.state.showMessage ? !this.state.error ?  this.context.intl.formatMessage({ id:'feedbackButton.feedbackSuccessTitle' }) : this.context.intl.formatMessage({ id:'feedbackButton.feedbackErrorTitle' }) : null}</strong>
                           {this.state.showMessage ? !this.state.error ? this.context.intl.formatMessage({ id:'feedbackButton.feedbackSuccess' }) : this.context.intl.formatMessage({ id:'feedbackButton.feedbackError' }) : null}
                          </p>
                          </div>

                        :
                          [
                            <textarea id="feedbackComments" onChange={this.handleTextAreaChange} style={{ resize: 'none' }}/>,
                            <button onClick={this.handleSubmit}> {this.context.intl.formatMessage({ id:'feedbackButton.sendButton' })}  </button>
                          ]

                        }
                        {<span onClick={this.handleRequestClose}>{this.context.intl.formatMessage({ id:'feedbackButton.cancel' })}</span>}
                    </Menu>
                </Popover>
            </div>
        );
    }
}

AdistecFeedbackButton.propTypes = propTypes;

export default injectIntl(AdistecFeedbackButton);
