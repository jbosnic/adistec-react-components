import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import uglify from 'rollup-plugin-uglify';
import commonjs from 'rollup-plugin-commonjs';
import { minify } from 'uglify-es';
import json from 'rollup-plugin-json';
import collectSass from 'rollup-plugin-collect-sass';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';

const name = 'adistec-react-components';
const path = 'dist/adistec-react-components';

const globals = {
	classnames: 'classNames',
	'prop-types': 'PropTypes',
	'react-dom': 'ReactDOM',
	react: 'React',
	'material-ui': 'material-ui'
};

const external = Object.keys(globals);
const babelOptions = () => {
	let result = {
		babelrc: false,
		presets: ['react','es2015-rollup', 'stage-0'],
		plugins: ['external-helpers'],
		ignore:['node_modules/**','examples/**']
	};
	result.plugins.push('transform-react-remove-prop-types');
	return result;
};

export default [
	{
		input: 'src/index.jsx',
		dest:path + '.js',
		output: {
			name: 'adistec-react-components',
			file: path + '.js',
			format: 'umd',
		},
		external: external,
		plugins: [
			peerDepsExternal({
				includeDependencies: true
			}),
			json({
		      include:'src/config/**',
		      preferConst: true, // Default: false
		      indent: '  '
		    }),
		   	collectSass({
		    	extensions:['.scss', '.sass','.css'],
		    	extract:'dist/adistec-react-components.css',
		    	importOnce:true
		    }),
			babel(babelOptions()),
			resolve({ jsnext: true,main: true,browser: true }),
			commonjs({
				extensions: [ '.js','.jsx','.json' ],
				exclude: ['src/examples/**'],
				include: ['node_modules/**']
			}),
			uglify({},minify)
		]
	}
];
